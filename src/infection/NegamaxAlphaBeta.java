package infection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe représentant l'algorithme NegamaxAlphaBeta
 *
 * @version 1.0
 */
public class NegamaxAlphaBeta extends AlphaBeta {

    public NegamaxAlphaBeta(int joueur) {
        super(joueur);
    }

    @Override
    public double alphaBeta(State etat, double alpha, double beta, int profondeur) {
        if (profondeur == 0 || etat.isOver()) {
            return evaluation(etat);
        } else {
            for (Move move : etat.getMove(etat.getJoueur())) {
                super.incrementNombreNoeud();
                State etatFutur = etat.play(move);
                alpha = Math.max(alpha, -alphaBeta(etatFutur, -beta, -alpha, profondeur - 1));
                if (alpha >= beta) {
                    return alpha;
                }
            }
            return alpha;
        }

    }

    @Override
    public Move getBestMove(State etat, int profondeur) {
        Move bestMove = null;
        double alpha = Double.NEGATIVE_INFINITY;
        double beta = Double.POSITIVE_INFINITY;
        List<Move> moves = etat.getMove(joueur);
        Collections.shuffle(moves);
        for (Move move : moves) {
            State etatFutur = etat.play(move);
            double value = -alphaBeta(etatFutur, beta, alpha, profondeur);
            if (value > alpha) {
                alpha = value;
                bestMove = move;
            }
            if (alpha >= beta) {
                return bestMove;
            }
        }
        return bestMove;
    }

}
