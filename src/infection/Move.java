package infection;

/**
 * Une classe representante un mouvement
 */
public class Move {

    /**
     * Le x de depart
     */
    private int x;

    /**
     * Le point y de depart
     */
    private int y;

    /**
     * Le x d'arrive
     */
    private int xFinal;

    /**
     * Le y d'arrive'
     */
    private int yFinal;

    /**
     * Le type de mouvement : simple(1) saut(2)
     */
    private int type;

    /**
     * 
     * @param x
     * @param y
     * @param xFinal
     * @param yFinal
     * @param type
     */

    public Move(int x, int y, int xFinal, int yFinal, int type) {
        this.x = x;
        this.y = y;
        this.xFinal = xFinal;
        this.yFinal = yFinal;
        this.type = type;
    }

    /**
     * Retourne le X de depart
     * 
     * @return un entier
     */
    public int getX() {
        return x;
    }

    /**
     * Retourne le Y de depart
     * 
     * @return un entier
     */
    public int getY() {
        return y;
    }

    /**
     * Retourne le X d'arrive
     * 
     * @return un entier
     */
    public int getXFinal() {
        return xFinal;
    }

    /**
     * Retourne le Y d'arrive
     * 
     * @return un entier
     */
    public int getYFinal() {
        return yFinal;
    }

    /**
     * Retourne le type de mouvement : simple(1) saut(2)
     * 
     * @return un entier
     */
    public int getType() {
        return type;
    }

    public boolean passeTour() {
        return x == xFinal && y == yFinal;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || !(obj instanceof Move)) {
            return false;
        }

        Move other = (Move) obj;

        if (type == 1) {
            return xFinal == other.xFinal && yFinal == other.yFinal;
        }
        return x == other.x && y == other.y && xFinal == other.xFinal && yFinal == other.yFinal;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (type != 1 ? 25072001 : 0);
        result = prime * result + (type == 1 ? 16012022 : (x ^ (x >>> 32)));
        result = prime * result + (type == 1 ? 28051996 : (y ^ (y >>> 32)));
        result = prime * result + (xFinal ^ (xFinal >>> 32));
        result = prime * result + (yFinal ^ (yFinal >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Move [type=" + type + "]: (" + x + ", " + y + ") -> (" + xFinal + ", " + yFinal + ")";
    }

}
