package infection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Classe représentant l'algorithme NegamaxMinimax
 *
 * @version 1.0
 */
public class NegamaxMinimax extends Minimax {

    public NegamaxMinimax(int joueur) {
        super(joueur);
    }

    @Override
    public double miniMax(State etat, int profondeur) {

        if (profondeur == 0 || etat.isOver()) {
            return evaluation(etat);
        }

        else {
            double m = Double.NEGATIVE_INFINITY;
            for (Move move : etat.getMove(etat.getJoueur())) {
                super.incrementNombreNoeud();
                State etatFutur = etat.play(move);
                m = Math.max(m, -miniMax(etatFutur, profondeur - 1));
            }
            return m;
        }

    }

    @Override
    public Move getBestMove(State etat, int profondeur) {
        Move bestMove = null;
        double bestvalue = Float.MIN_VALUE;
        List<Move> moves = etat.getMove(joueur);
        Collections.shuffle(moves);
        for (Move move : moves) {
            State etatFutur = etat.play(move);
            double value = -miniMax(etatFutur, profondeur);
            if (value > bestvalue) {
                bestvalue = value;
                bestMove = move;
            }
        }
        return bestMove;
    }

}
