package infection;

public class Main {

    public static void main(String[] args) {
        int profondeurBleu;
        int profondeurRouge;
        boolean elalage;
        Algorithm bleu;
        Algorithm rouge;
        if (args.length >= 3) {
            profondeurBleu = Integer.parseInt(args[0]);
            profondeurRouge = Integer.parseInt(args[1]);
            elalage = Boolean.parseBoolean(args[2]);
        } else {
            profondeurBleu = 1;
            profondeurRouge = 1;
            elalage = true;
        }

        if (elalage) {
            bleu = new AlphaBeta(0);
            rouge = new AlphaBeta(1);
        } else {
            bleu = new Minimax(0);
            rouge = new Minimax(1);
        }
        State etat = new State();
        Move move = null;
        System.out.println("Debut de la partie");
        System.out.println(etat);

        while (!etat.isOver()) {
            if (etat.getJoueur() == 0) {
                move = bleu.getBestMove(etat, profondeurBleu);
            } else if (etat.getJoueur() == 1) {
                move = rouge.getBestMove(etat, profondeurRouge);
            } else {
                System.out.println("Erreur dans l'etat.");
                break;
            }
            etat = etat.play(move);
            System.out.println(etat);

            System.out.println("Score joueur rouge :" + etat.getScore(1));
            System.out.println("Score joueur bleu :" + etat.getScore(0));
        }
        System.out.println(etat);
        System.out.println("\nFin de la partie");
        System.out.println("Score joueur rouge :" + etat.getScore(1));
        System.out.println("Score joueur bleu :" + etat.getScore(0));
        System.out.println("Nombre de noeuds visité : " + bleu.getNombreNoeud());

    }
}
