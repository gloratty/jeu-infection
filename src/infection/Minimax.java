package infection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class representant un algorithm minimax
 *
 * @version 1.0
 */
public class Minimax extends Algorithm {

    /**
     * Construit une nouvelle instance de minimax
     */
    public Minimax(int joueur) {
        super(joueur);
    }

    /**
     * Algorithm minimax evaluant un etat
     * 
     * @param etat       l'etat a evalue
     * @param profondeur la profondeur de recherche
     * @return un réel indiquant la valeur de l'etat
     */
    public double miniMax(State etat, int profondeur) {
        super.incrementNombreNoeud();
        if (profondeur == 0 || etat.isOver()) {
            return etat.getScore(joueur);
        } else {
            double b;
            if (joueur == etat.getJoueur()) {
                b = Float.MIN_VALUE;
                for (Move move : etat.getMove(etat.getJoueur())) {
                    State etatFutur = etat.play(move);
                    b = Math.max(b, miniMax(etatFutur, profondeur - 1));

                }

            } else {
                b = Float.MAX_VALUE;
                for (Move move : etat.getMove(etat.getJoueur())) {
                    State etatFutur = etat.play(move);
                    b = Math.min(b, miniMax(etatFutur, profondeur - 1));
                }
            }

            return b;
        }
    }

    @Override
    public Move getBestMove(State etat, int profondeur) {
        Move bestMove = null;
        double bestvalue = Float.MIN_VALUE;
        List<Move> moves = etat.getMove(joueur);
        Collections.shuffle(moves);
        for (Move move : moves) {
            State etatFutur = etat.play(move);
            double value = miniMax(etatFutur, profondeur);
            if (value > bestvalue) {
                bestvalue = value;
                bestMove = move;
            }
        }
        return bestMove;
    }
}
