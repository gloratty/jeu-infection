package infection;

import javax.swing.event.*;
import javax.swing.*;
import javax.swing.border.Border;

import java.awt.event.*;
import java.awt.*;

/**
 * Classe Case représentant une case du plateau de jeu
 *
 * @version 1.0
 */
public class Case extends JPanel {

    private int x;
    private int y;

    public Case(int x, int y) {
        this.x = x;
        this.y = y;
        this.setPreferredSize(new Dimension(15, 15));
        this.setBackground(Color.black);
        Border border = BorderFactory.createLineBorder(Color.white, 3);
        this.setBorder(border);
    }

    public void setColor(int pion) {
        if (pion == -1) {
            this.setBackground(Color.black);
        } else if (pion == 0) {
            this.setBackground(Color.blue);
        } else if (pion == 1) {
            this.setBackground(Color.red);
        }
    }

}
