package infection;

/**
 * Class enumerative qui enumere les differentes representation des pions dans le tableau
 */
public enum Representation {
    
    X(-1), R(1), B(0);

    /**
     * L'entier representant un pion
     */
    private int abreviation;

    private Representation(int abreviation) {
        this.abreviation = abreviation;
    }

    public int getAbreviation() {
        return this.abreviation;
    }

    

    public static Representation getRepresentation(int value) {
        for (Representation r : Representation.values()) {
            if (r.getAbreviation() == value) {
                return r;
            }
        }
        return null;
    }

    
}
