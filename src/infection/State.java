package infection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class representante un etat de jeu
 * 
 * @version 1.0
 */

public class State {

    /**
     * La grille de jeu
     */
    private int[][] grille;

    /**
     * Le joueur actif
     */
    private int joueur;

    /**
     * La liste de pion du joueur rouge
     */
    private Set<Integer> pionRouge;

    /**
     * La liste de pion bleu du joueur rouge
     */
    private Set<Integer> pionBleu;

    private final int[][] GRILLE_INIT = {
            { 1, -1, -1, -1, -1, -1, 0 },
            { -1, -1, -1, -1, -1, -1, -1 },
            { -1, -1, -1, -1, -1, -1, -1 },
            { -1, -1, -1, -1, -1, -1, -1 },
            { -1, -1, -1, -1, -1, -1, -1 },
            { -1, -1, -1, -1, -1, -1, -1 },
            { 0, -1, -1, -1, -1, -1, 1 }
    };
    private List<State> etats;

    /**
     * Construit une nouvelle instance d'un Etat
     * 
     * @param grille    la grille de jeu
     * @param joueur    le joueur actuellement actif
     * @param pionRouge le liste de pion rouge sur la grille
     * @param pionBleu  La liste de pion bleu du joueur rouge
     */
    public State(int[][] grille, int joueur, Set<Integer> pionRouge, Set<Integer> pionBleu, List<State> etats) {
        this.grille = grille;
        this.joueur = joueur;
        this.pionRouge = pionRouge;
        this.pionBleu = pionBleu;
        this.etats = etats;
    }

    /**
     * Construit une nouvelle instance d'un etat par défaut
     */
    public State() {
        this.grille = GRILLE_INIT;
        this.joueur = 0;
        this.pionRouge = new HashSet<>();
        this.pionRouge.add(0);
        this.pionRouge.add(48);
        this.pionBleu = new HashSet<>();
        this.pionBleu.add(6);
        this.pionBleu.add(42);
        this.etats = new ArrayList<>();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        State etat = (State) obj;
        return this.joueur == etat.joueur
                && (etat.getPionRouge().containsAll(this.pionRouge)
                        && this.pionRouge.containsAll(etat.getPionRouge()))
                && (etat.getPionBleu().containsAll(this.pionBleu)
                        && this.pionBleu.containsAll(etat.getPionBleu()));

    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                sb.append("|" + Representation.getRepresentation(grille[i][j]));
            }
            sb.append("|\n");
        }
        return sb.toString();
    }

    /**
     * Retourne la liste de tous les états précédents
     * 
     * @return une liste d'etat
     */

    public List<State> getEtatsPrecedent() {
        List<State> res = new ArrayList<>();
        res.addAll(etats);
        return res;
    }

    /**
     * Retourne la grille de jeu
     * 
     * @return un tableau 2D
     */
    public int[][] getGrille() {
        return grille;
    }

    /**
     * Retourne le joueur actuellement actif
     * 
     * @return un entier indiquant le numero du joueur
     */
    public int getJoueur() {
        return joueur;
    }

    /**
     * Retourne le liste de pion du joueur rouge
     * 
     * @return un set representant la liste de pion du joueur rouge
     */
    public Set<Integer> getPionRouge() {
        return pionRouge;
    }

    /**
     * Retourne la liste de pion du joueur bleu
     * 
     * @return set representant la liste de pion du joueur bleu
     */
    public Set<Integer> getPionBleu() {
        return pionBleu;
    }

    /**
     * Compte le nombre de case de la grille contenant une valeur x
     * 
     * @param x la valeur recherche
     * @return un entier representant le nombre de case contenant x
     */
    public float getNombreCase(int x) {
        float res = 0;
        for (int i = 0; i < grille.length; i++) {
            for (int j = 0; j < grille[i].length; j++) {
                res = grille[i][j] == x ? (res + 1) : res;
            }
        }
        return res;
    }

    /**
     * Retourne le score du joueur actif
     * 
     * @param joueur
     * @return un reel indiquant le score du joueur actif
     */
    public double getScore(int joueur) {
        return (joueur == 0 ? pionBleu.size() : pionRouge.size()) / (double) (pionBleu.size() + pionRouge.size());
    }

    /**
     * Retourne un boolean indiquant si une case est dans la grille
     * 
     * @return un boolean indiquant si une case est dans la grille
     */
    public boolean estDansLesLimites(int x, int y) {
        return (x >= 0 && x < 7) && (y >= 0 && y < 7);
    }

    /**
     * Retourne un boolean indiquant si une case est vide
     * 
     * @param x
     * @param y
     * @return un boolean
     */
    public boolean estVide(int x, int y) {
        return estVide(grille, x, y);
    }

    public boolean estVide(int[][] g, int x, int y) {
        return g[x][y] == -1;
    }

    /**
     * Retourne l'ensemble des mouvements possible d'un pion
     * 
     * @param x le numero de la ligne du pion
     * @param y le numero de la colonne du pion
     * 
     * @return un ensemble des mouvements possible
     */
    public Set<Move> possible(int x, int y) {
        Set<Move> mouvementPossible = new HashSet<>();
        mouvementPossible.addAll(possible(x, y, 2));
        mouvementPossible.addAll(possible(x, y, 1));

        return mouvementPossible;
    }

    /**
     * Retourne l'ensemble des mouvements possible d'un pion suivant un type de
     * deplacement
     * 
     * @param x    le numero de la ligne du pion
     * @param y    le numero de la colonne du pion
     * @param type le type de deplace du pion
     * @return un ensemble des mouvements possible
     */
    public Set<Move> possible(int x, int y, int type) {
        Set<Move> mouvementPossible = new HashSet<>();
        int xd;
        int yd;
        for (int dx = -type; dx <= type; dx += type) {
            for (int dy = -type; dy <= type; dy += type) {
                xd = x + dx;
                yd = y + dy;
                if (estDansLesLimites(xd, yd) && estVide(xd, yd)) {
                    mouvementPossible.add(new Move(x, y, xd, yd, type));
                }
            }
        }
        return mouvementPossible;
    }

    /**
     * Retourne l'ensemble des pions d'un joueur
     * 
     * @param joueur
     * @return une liste de mouvements
     */
    public List<Move> getMove(int joueur) {
        Set<Integer> pions = joueur == 1 ? pionRouge : pionBleu;
        Set<Move> mouvementPossible = new HashSet<>();
        pions.forEach((Integer pion) -> {
            int[] coord = coordonnees(pion);
            mouvementPossible.addAll(possible(coord[0], coord[1]));
        });
        List<Move> mouvements = new ArrayList<>(mouvementPossible);
        // Le null represente un coup passe. On le rajoute au milieu de l'ensemble
        mouvements.add(mouvements.size() / 2, null);
        return mouvements;
    }

    /**
     * Retourne le numero d'une case
     * 
     * @param x le numero de la ligne
     * @param y le numero de la colonne
     * 
     * @return un entier representant le numero de la case
     */
    public int numero(int x, int y) {
        return x * 7 + y;
    }

    /**
     * Retourne le numero d'une case
     * 
     * @param tab tableau representant les coordonnees d'une case
     * 
     * @return un entier representant le numero de la case
     */
    public int numero(int[] tab) {
        return numero(tab[0], tab[1]);
    };

    /**
     * Retourne le numero de la ligne et de la colonne d'une case
     * 
     * @param numeroCasele numero de la la case
     * @return un tableau 1D
     */
    public int[] coordonnees(int numeroCase) {
        int[] res = { numeroCase / 7, numeroCase % 7 };
        return res;
    }

    /**
     * Return une copie de la grille
     * 
     * @return un tableau 2D
     */
    public int[][] cloneGrille() {
        int[][] grilleCopie = new int[7][7];
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                grilleCopie[i][j] = grille[i][j];
            }
        }
        return grilleCopie;
    }

    /**
     * Retourne un nouvelle etat de jeu
     * 
     * @return un nouvelle etat de jeu
     */
    public State play(Move move) {
        int[][] grilleCopie = cloneGrille();
        Set<Integer> pionsRougeCopie = new HashSet<>();

        pionsRougeCopie.addAll(pionRouge);
        Set<Integer> pionsBleuCopie = new HashSet<>();
        pionsBleuCopie.addAll(pionBleu);

        List<State> etatsCopy = new ArrayList<>();
        etatsCopy.addAll(etats);
        etatsCopy.add(this);
        if (move == null) {
            return new State(grilleCopie, (joueur + 1) % 2, pionsRougeCopie, pionsBleuCopie, etatsCopy);
        }

        int[] pion = { move.getX(), move.getY() };
        int[] pionArrive = { move.getXFinal(), move.getYFinal() };

        int joueurActuel = grille[pion[0]][pion[1]];

        grilleCopie[pionArrive[0]][pionArrive[1]] = joueurActuel;
        Set<Integer> pionsJ = joueurActuel == 0 ? pionsBleuCopie : pionsRougeCopie;
        Set<Integer> pionsA = joueurActuel == 0 ? pionsRougeCopie : pionsBleuCopie;
        pionsJ.add(numero(pionArrive));
        if (move.getType() == 2) {
            grilleCopie[pion[0]][pion[1]] = -1;
            pionsJ.remove(numero(pion));
        }

        int p;
        for (int i = -1; i <= 1; i++) {
            for (int j = -1; j <= 1; j++) {
                if (estDansLesLimites(move.getXFinal() + i, move.getYFinal() + j)
                        && !estVide(grilleCopie, move.getXFinal() + i, move.getYFinal() + j)) {
                    p = numero(move.getXFinal() + i, move.getYFinal() + j);
                    grilleCopie[move.getXFinal() + i][move.getYFinal() + j] = joueurActuel;
                    pionsJ.add(p);
                    pionsA.remove(p);
                }
            }
        }

        return new State(grilleCopie, (joueurActuel + 1) % 2, pionsRougeCopie, pionsBleuCopie, etatsCopy);
    }

    public boolean isOver() {
        return pionBleu.isEmpty() || pionRouge.isEmpty() || ((pionBleu.size() + pionRouge.size()) == 49)
                || etats.contains(this);
    }

}
