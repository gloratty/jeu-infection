package infection;

/**
 * Une classe representant les méthodes communes à tous les algorithmes
 */
public abstract class Algorithm {

    /**
     * Le joueur actuellement actif
     */
    protected int joueur;

    /**
     * Le nombre de noeud parcouru
     */
    protected int nombreNoeud;

    protected Algorithm(int joueur) {
        this.joueur = joueur;
        nombreNoeud = 0;
    }

    /**
     * Retourne le joueur actuel
     * 
     * @return un entier
     */
    public int getJoueur() {
        return joueur;
    }

    /**
     * Retourne le nombre de noeud exploré
     * 
     * @return un entier
     */
    public int getNombreNoeud() {
        return nombreNoeud;
    }

    /**
     * Increment le nombre de noeud de exploré
     */
    protected void incrementNombreNoeud() {
        nombreNoeud++;
    }

    /**
     * Renvoie l'evaluation d'un etat
     * 
     * @param etat
     * @return un reel
     */
    public double evaluation(State etat) {
        if (etat.getJoueur() == joueur) {
            return etat.getScore(joueur);
        }
        return -etat.getScore(joueur);
    }

    /**
     * Retourne le meilleur coup pouvant etre jouer
     * 
     * @parm profondeur
     * @return un mouvement
     */
    public abstract Move getBestMove(State etat, int profondeur);
}
