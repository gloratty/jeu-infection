package infection;

import java.util.Collections;
import java.util.List;

/**
 * Classe AlphaBeta qui permet de calculer le meilleur coup à jouer
 *
 * @version 1.0
 *
 */
public class AlphaBeta extends Algorithm {

    public AlphaBeta(int joueur) {
        super(joueur);
    }

    /**
     * Algorithm alphabeta evaluant un etat
     * 
     * @param etat       l'etat a evalue
     * @param alpha      valeur la plus basse que le joueur Max sait pouvoir obtenir
     * @param beta       valeur maximale que le joueur Min autorisera Max à obtenir
     * @param profondeur la profondeur de recherche
     * @return un réel indiquant la valeur de l'etat
     */
    public double alphaBeta(State etat, double alpha, double beta, int profondeur) {
        super.incrementNombreNoeud();
        if (profondeur == 0 || etat.isOver()) {
            return etat.getScore(joueur);
        } else {
            if (etat.getJoueur() == joueur) {
                for (Move move : etat.getMove(etat.getJoueur())) {

                    State etatFutur = etat.play(move);
                    alpha = Math.max(alpha, alphaBeta(etatFutur, alpha, beta, profondeur - 1));
                    if (alpha >= beta) {
                        return alpha;
                    }
                }
                return alpha;
            } else {
                for (Move move : etat.getMove(etat.getJoueur())) {
                    State etatFutur = etat.play(move);
                    beta = Math.min(beta, alphaBeta(etatFutur, alpha, beta, profondeur - 1));
                    if (alpha >= beta) {
                        return beta;
                    }
                }
                return beta;
            }
        }

    }

    @Override
    public Move getBestMove(State etat, int profondeur) {
        Move bestMove = null;
        double alpha = Double.NEGATIVE_INFINITY;
        double beta = 1;
        List<Move> moves = etat.getMove(joueur);
        Collections.shuffle(moves);
        for (Move move : moves) {
            State etatFutur = etat.play(move);
            double value = alphaBeta(etatFutur, alpha, beta, profondeur);
            if (value > alpha) {
                alpha = value;
                bestMove = move;
            }
            if (alpha >= beta) {
                return move;
            }
        }

        return bestMove;
    }
}
