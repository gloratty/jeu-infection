package infection;

import javax.swing.event.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * Classe représentant l'interface graphique de l'application
 *
 * @version 1.0
 */
public class Fenetre extends JFrame {

    public Case[][] cases;
    State etat;

    public Fenetre() {
        super("Jeux d'infection");
        WindowListener l = new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        };
        cases = new Case[7][7];
        GridLayout layout = new GridLayout(7, 7, 2, 2);
        this.setLayout(layout);
        addWindowListener(l);
        setMinimumSize(new Dimension(350, 350));
        // setSize(1200, 800);
        setVisible(true);

        this.etat = new State();
        Algorithm alg = new AlphaBeta(0);
        Algorithm alg2 = new AlphaBeta(1);
        this.addKeyListener(new KeyListener() {

            @Override
            public void keyPressed(KeyEvent e) {
                // TODO Auto-generated method stub
                if (e.getKeyCode() == 65 && etat.getJoueur() == 0 && !etat.isOver()) {
                    etat = etat.play(alg.getBestMove(etat, 2));
                }

                else if (e.getKeyCode() == 66 && etat.getJoueur() == 1 && !etat.isOver()) {
                    etat = etat.play(alg2.getBestMove(etat, 2));
                } else if (e.getKeyCode() == 82) {
                    etat = new State();
                }
                for (int i = 0; i < 7; i++) {
                    for (int j = 0; j < 7; j++) {
                        cases[i][j].setColor(etat.getGrille()[i][j]);
                    }
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void keyTyped(KeyEvent e) {

            }

        });

        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                Case panel = new Case(i, j);
                cases[i][j] = panel;
                this.add(panel, BorderLayout.CENTER);
                cases[i][j].setColor(etat.getGrille()[i][j]);
            }
        }

    }

    public static void main(String[] args) {
        JFrame frame = new Fenetre();
    }
}
